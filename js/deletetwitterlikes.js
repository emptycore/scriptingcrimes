function nextUnlike() {
  return document.querySelector('[data-testid="unlike"]');
}

function wait(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function removeAll() {
  let count = 0;
  let next = nextUnlike();
  while (next && count < 500) {
    try {
      next.focus();
      next.click();
      console.log(`Unliked ${++count} tweets`);
      await wait(count % 50 === 0 ? 60000 : 10000); // Increased wait times
      next = nextUnlike();
      if (!next && count < 500) {
        window.scrollTo(0, document.body.scrollHeight); // Scroll to the bottom of the page
        await wait(10000); // Increased wait time for more tweets to load
        next = nextUnlike(); // Get the next unlike button
      }
    } catch (error) {
      console.error('An error occurred:', error);
      break;
    }
  }
  if (next) {
    console.log('Finished early to prevent rate-limiting');
  } else {
    console.log('Finished, count =', count);
  }
}

removeAll();
